// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKEGAME_001_Interactable_generated_h
#error "Interactable.generated.h already included, missing '#pragma once' in Interactable.h"
#endif
#define SNAKEGAME_001_Interactable_generated_h

#define SnakeGame_002_Source_SnakeGame_001_Interactable_h_13_SPARSE_DATA
#define SnakeGame_002_Source_SnakeGame_001_Interactable_h_13_RPC_WRAPPERS
#define SnakeGame_002_Source_SnakeGame_001_Interactable_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define SnakeGame_002_Source_SnakeGame_001_Interactable_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	SNAKEGAME_001_API UInteractable(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UInteractable) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(SNAKEGAME_001_API, UInteractable); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UInteractable); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	SNAKEGAME_001_API UInteractable(UInteractable&&); \
	SNAKEGAME_001_API UInteractable(const UInteractable&); \
public:


#define SnakeGame_002_Source_SnakeGame_001_Interactable_h_13_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	SNAKEGAME_001_API UInteractable(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	SNAKEGAME_001_API UInteractable(UInteractable&&); \
	SNAKEGAME_001_API UInteractable(const UInteractable&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(SNAKEGAME_001_API, UInteractable); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UInteractable); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UInteractable)


#define SnakeGame_002_Source_SnakeGame_001_Interactable_h_13_GENERATED_UINTERFACE_BODY() \
private: \
	static void StaticRegisterNativesUInteractable(); \
	friend struct Z_Construct_UClass_UInteractable_Statics; \
public: \
	DECLARE_CLASS(UInteractable, UInterface, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Interface), CASTCLASS_None, TEXT("/Script/SnakeGame_001"), SNAKEGAME_001_API) \
	DECLARE_SERIALIZER(UInteractable)


#define SnakeGame_002_Source_SnakeGame_001_Interactable_h_13_GENERATED_BODY_LEGACY \
		PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	SnakeGame_002_Source_SnakeGame_001_Interactable_h_13_GENERATED_UINTERFACE_BODY() \
	SnakeGame_002_Source_SnakeGame_001_Interactable_h_13_STANDARD_CONSTRUCTORS \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SnakeGame_002_Source_SnakeGame_001_Interactable_h_13_GENERATED_BODY \
	PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	SnakeGame_002_Source_SnakeGame_001_Interactable_h_13_GENERATED_UINTERFACE_BODY() \
	SnakeGame_002_Source_SnakeGame_001_Interactable_h_13_ENHANCED_CONSTRUCTORS \
private: \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SnakeGame_002_Source_SnakeGame_001_Interactable_h_13_INCLASS_IINTERFACE_NO_PURE_DECLS \
protected: \
	virtual ~IInteractable() {} \
public: \
	typedef UInteractable UClassType; \
	typedef IInteractable ThisClass; \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define SnakeGame_002_Source_SnakeGame_001_Interactable_h_13_INCLASS_IINTERFACE \
protected: \
	virtual ~IInteractable() {} \
public: \
	typedef UInteractable UClassType; \
	typedef IInteractable ThisClass; \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define SnakeGame_002_Source_SnakeGame_001_Interactable_h_10_PROLOG
#define SnakeGame_002_Source_SnakeGame_001_Interactable_h_21_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGame_002_Source_SnakeGame_001_Interactable_h_13_SPARSE_DATA \
	SnakeGame_002_Source_SnakeGame_001_Interactable_h_13_RPC_WRAPPERS \
	SnakeGame_002_Source_SnakeGame_001_Interactable_h_13_INCLASS_IINTERFACE \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SnakeGame_002_Source_SnakeGame_001_Interactable_h_21_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGame_002_Source_SnakeGame_001_Interactable_h_13_SPARSE_DATA \
	SnakeGame_002_Source_SnakeGame_001_Interactable_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	SnakeGame_002_Source_SnakeGame_001_Interactable_h_13_INCLASS_IINTERFACE_NO_PURE_DECLS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKEGAME_001_API UClass* StaticClass<class UInteractable>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SnakeGame_002_Source_SnakeGame_001_Interactable_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
