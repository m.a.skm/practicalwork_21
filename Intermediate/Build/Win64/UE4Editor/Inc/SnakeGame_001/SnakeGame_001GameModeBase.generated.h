// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKEGAME_001_SnakeGame_001GameModeBase_generated_h
#error "SnakeGame_001GameModeBase.generated.h already included, missing '#pragma once' in SnakeGame_001GameModeBase.h"
#endif
#define SNAKEGAME_001_SnakeGame_001GameModeBase_generated_h

#define SnakeGame_002_Source_SnakeGame_001_SnakeGame_001GameModeBase_h_15_SPARSE_DATA
#define SnakeGame_002_Source_SnakeGame_001_SnakeGame_001GameModeBase_h_15_RPC_WRAPPERS
#define SnakeGame_002_Source_SnakeGame_001_SnakeGame_001GameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define SnakeGame_002_Source_SnakeGame_001_SnakeGame_001GameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASnakeGame_001GameModeBase(); \
	friend struct Z_Construct_UClass_ASnakeGame_001GameModeBase_Statics; \
public: \
	DECLARE_CLASS(ASnakeGame_001GameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGame_001"), NO_API) \
	DECLARE_SERIALIZER(ASnakeGame_001GameModeBase)


#define SnakeGame_002_Source_SnakeGame_001_SnakeGame_001GameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesASnakeGame_001GameModeBase(); \
	friend struct Z_Construct_UClass_ASnakeGame_001GameModeBase_Statics; \
public: \
	DECLARE_CLASS(ASnakeGame_001GameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGame_001"), NO_API) \
	DECLARE_SERIALIZER(ASnakeGame_001GameModeBase)


#define SnakeGame_002_Source_SnakeGame_001_SnakeGame_001GameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASnakeGame_001GameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASnakeGame_001GameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnakeGame_001GameModeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnakeGame_001GameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnakeGame_001GameModeBase(ASnakeGame_001GameModeBase&&); \
	NO_API ASnakeGame_001GameModeBase(const ASnakeGame_001GameModeBase&); \
public:


#define SnakeGame_002_Source_SnakeGame_001_SnakeGame_001GameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASnakeGame_001GameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnakeGame_001GameModeBase(ASnakeGame_001GameModeBase&&); \
	NO_API ASnakeGame_001GameModeBase(const ASnakeGame_001GameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnakeGame_001GameModeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnakeGame_001GameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASnakeGame_001GameModeBase)


#define SnakeGame_002_Source_SnakeGame_001_SnakeGame_001GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define SnakeGame_002_Source_SnakeGame_001_SnakeGame_001GameModeBase_h_12_PROLOG
#define SnakeGame_002_Source_SnakeGame_001_SnakeGame_001GameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGame_002_Source_SnakeGame_001_SnakeGame_001GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	SnakeGame_002_Source_SnakeGame_001_SnakeGame_001GameModeBase_h_15_SPARSE_DATA \
	SnakeGame_002_Source_SnakeGame_001_SnakeGame_001GameModeBase_h_15_RPC_WRAPPERS \
	SnakeGame_002_Source_SnakeGame_001_SnakeGame_001GameModeBase_h_15_INCLASS \
	SnakeGame_002_Source_SnakeGame_001_SnakeGame_001GameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SnakeGame_002_Source_SnakeGame_001_SnakeGame_001GameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGame_002_Source_SnakeGame_001_SnakeGame_001GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	SnakeGame_002_Source_SnakeGame_001_SnakeGame_001GameModeBase_h_15_SPARSE_DATA \
	SnakeGame_002_Source_SnakeGame_001_SnakeGame_001GameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	SnakeGame_002_Source_SnakeGame_001_SnakeGame_001GameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	SnakeGame_002_Source_SnakeGame_001_SnakeGame_001GameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKEGAME_001_API UClass* StaticClass<class ASnakeGame_001GameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SnakeGame_002_Source_SnakeGame_001_SnakeGame_001GameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
